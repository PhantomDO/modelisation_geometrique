﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    [System.Serializable]
    public struct Voxel
    {
        public Vector3[] Vertices;
        public int[] Triangles;
        
        public Vector3 Center;
        public Vector3 Width;
        
        public Voxel(Vector3 center, Vector3 width)
        {
            Width = width;
            Center = center;
            Vertices = new Vector3[6 * 4];
            Triangles = new int[12 * 3];
            Update(center, width);
        }

        public void Update(Vector3 center, Vector3 width)
        {
            Center = center;
            Width = width;

            Vector3 wN = -Width / 2;
            Vector3 wP = Width / 2;

            var v = new Vector3[]
            {
                /*Center + */new Vector3(wN.x, wN.y, wN.z),
                /*Center + */new Vector3(wN.x, wP.y, wN.z),
                /*Center + */new Vector3(wP.x, wP.y, wN.z),
                /*Center + */new Vector3(wP.x, wN.y, wN.z),
                /*Center + */new Vector3(wN.x, wN.y, wP.z),
                /*Center + */new Vector3(wN.x, wP.y, wP.z),
                /*Center + */new Vector3(wP.x, wP.y, wP.z),
                /*Center + */new Vector3(wP.x, wN.y, wP.z)
            };

            Vertices = new Vector3[]
            {
                v[0],v[1],v[2],v[3],//devant
                v[4],v[5],v[1],v[0],//gauche
                v[3],v[2],v[6],v[7],//Droite
                v[7],v[6],v[5],v[4],//Derrière
                v[1],v[5],v[6],v[2],//Dessus
                v[4],v[0],v[3],v[7] //dessous
            };

            for (int i = 0, n = 0; i < 6; i++)
            {
                //triangle1
                Triangles[n++] = i * 4;
                Triangles[n++] = i * 4 + 1;
                Triangles[n++] = i * 4 + 3;

                //triangle2
                Triangles[n++] = i * 4 + 1;
                Triangles[n++] = i * 4 + 2;
                Triangles[n++] = i * 4 + 3;
            }
        }

        public static bool IsVisible(MeshFilter[] meshs, Func<MeshFilter[], bool> visibilityFunc)
        {
            return visibilityFunc != null && visibilityFunc.Invoke(meshs);
        }

        public GameObject GetVoxel(int index, GameObject cube)
        {
            if (cube == null)
            {
                cube = new GameObject($"Voxel ({index})", 
                    new[] {typeof(MeshFilter), typeof(MeshRenderer), typeof(BoxCollider)});
            }

            if (cube.TryGetComponent(out MeshFilter meshFilter))
            {
                meshFilter.mesh.vertices = Vertices;
                meshFilter.mesh.triangles = Triangles;
                meshFilter.mesh.RecalculateNormals();
            }

            return cube;
        }
    }
}