﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Drill : MonoBehaviour
    {
        [SerializeField] private Camera _mainCamera;
        [SerializeField] private Voxelizer _voxelizer;
        [SerializeField] private float _moveSpeed = 5.0f;
        [SerializeField] private float _lookSpeed = 5.0f;
        [SerializeField] private float _lookXLimit = 45.0f;
        [SerializeField] private float _potential = 0.02f;
        
        private float _rotationX;

        private LineRenderer _lineRenderer;

        private void Start()
        {
            if (TryGetComponent(out LineRenderer lr)) _lineRenderer = lr; 

            Cursor.lockState = CursorLockMode.Confined;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Cursor.lockState = Cursor.lockState == CursorLockMode.None 
                    ? CursorLockMode.Confined : CursorLockMode.None;
            }

            Cursor.visible = Cursor.lockState == CursorLockMode.None;

            Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0.0f,  Input.GetAxis("Vertical"));
            Vector3 direction = (_mainCamera.transform.forward * movement.z) + (transform.right * movement.x);
            
            if (movement.sqrMagnitude > 0.0f)
            {
                transform.position += direction * _moveSpeed * Time.deltaTime;
            }

            if (Cursor.lockState != CursorLockMode.None)
            {
                _rotationX += -Input.GetAxis("Mouse Y") * _lookSpeed;
                _rotationX = Mathf.Clamp(_rotationX, -_lookXLimit, _lookXLimit);
                _mainCamera.transform.localRotation = Quaternion.Euler(_rotationX, 0, 0);
                transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * _lookSpeed, 0);
            }

            var ray = _mainCamera.ScreenPointToRay(new Vector3(
                Mathf.Floor(Screen.width / 2.0f), 
                Mathf.Floor(Screen.height / 2.0f), 
                _mainCamera.farClipPlane));
            

            if (_voxelizer && Physics.Raycast(ray, out RaycastHit hit, 200.0f))
            {
                Debug.DrawLine(_mainCamera.transform.position, hit.point, Color.red);
                if (Input.GetMouseButton(0))
                {
                    _voxelizer.AddPotential(hit.point, _potential);
                }

                if (Input.GetMouseButton(1))
                {
                    _voxelizer.RemovePotential(hit.point, _potential);
                }

                if (_lineRenderer && TryGetComponent(out CapsuleCollider col))
                {
                    float dist = Mathf.Abs(_mainCamera.transform.position.y - (transform.position.y + col.bounds.max.y));
                    _lineRenderer.SetPosition(0, transform.position + (Vector3.up * (dist / 2)));
                    _lineRenderer.SetPosition(1, Input.GetMouseButton(0) || Input.GetMouseButton(1) 
                        ? hit.point : transform.position + (Vector3.up * (dist / 2)));
                }
            }
        }
    }
}