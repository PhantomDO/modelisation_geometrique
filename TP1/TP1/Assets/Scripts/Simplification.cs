﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    [System.Serializable]
    public class IndexBounds : SerializableDictionary<int, Bounds> {}
    
    [System.Serializable]
    public class IndexListStorage : SerializableDictionary.Storage<List<int>> {}

    [System.Serializable]
    public class BoundsVertices : SerializableDictionary<Bounds, List<int>, IndexListStorage> {}

    struct Triangle
    {
        public Vector3Int VerticesIndex;
    }

    public class Simplification : MonoBehaviour
    {
        public MeshFilter meshFilter;
        public Vector3Int gridSize;

        private bool swap;
        
        private IndexBounds indexBounds;
        private BoundsVertices boundsVertices;
        
        private Mesh baseMesh;
        private Mesh simplifiedMesh;

        private Vector3Int oldGridSize;

        private List<Vector3> nVertices = new List<Vector3>();
        private List<int> nTriangles = new List<int>();

        public void Start()
        {
            indexBounds = new IndexBounds();
            boundsVertices = new BoundsVertices();
            CreateGrid3D();
            Simplify();
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.G))
            {
                CreateGrid3D();
                Simplify();
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                if (meshFilter == null && TryGetComponent(out MeshFilter mf))
                {
                    meshFilter = mf;
                    CreateGrid3D();
                    Simplify();
                }

                if (nVertices.Count <= 0 && nTriangles.Count <= 0)
                {
                    CreateGrid3D();
                    Simplify();
                }

                if (meshFilter != null)
                {
                    swap = !swap;
                    meshFilter.sharedMesh = swap ? simplifiedMesh : baseMesh;
                }
            }
        }

        public void OnValidate()
        {
            //if (gridSize.sqrMagnitude != oldGridSize.sqrMagnitude)
            //{
            //    CreateGrid3D();
            //    Simplify();
            //}
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (indexBounds != null && indexBounds.Count > 0 && boundsVertices != null && boundsVertices.Count > 0)
            {
                Gizmos.color = Color.green;
                foreach (var kvp in indexBounds)
                {
                    if (boundsVertices.ContainsKey(kvp.Value) && 
                        boundsVertices[kvp.Value] != null && boundsVertices[kvp.Value].Count > 0)
                    {
                        UnityEditor.Handles.Label(transform.position + kvp.Value.center, $"{kvp.Key}");
                        Gizmos.DrawWireCube(transform.position + kvp.Value.center, kvp.Value.size);
                    }
                }
            }
        }
#endif

        public int[] TrianglesFromVertex(int index)
        {
            var triangles = meshFilter.sharedMesh.triangles;
            
            List<int> trianglesFromVertex = new List<int>();
            StringBuilder sb = new StringBuilder();

            sb.Append($"Vertices[{index}] triangles are : ");

            for (int i = 0; i < triangles.Length; i += 3)
            {
                if (triangles[i + 0] == index) trianglesFromVertex.Add(i);
                if (triangles[i + 1] == index) trianglesFromVertex.Add(i);
                if (triangles[i + 2] == index) trianglesFromVertex.Add(i);
            }

            for (int i = 0; i < trianglesFromVertex.Count; i++)
            {
                sb.Append($"{trianglesFromVertex[i]}");
                if (i < trianglesFromVertex.Count - 1) sb.Append($", ");
            }

            Debug.Log(sb.ToString());

            return trianglesFromVertex.ToArray();
        }

        public int[] NeighboorsFromVertex(int index)
        {
            var triangles = meshFilter.sharedMesh.triangles;

            var fromVertex = TrianglesFromVertex(index);
            List<int> neighboors = new List<int>();

            foreach (var i in fromVertex)
            {
                if (!neighboors.Contains(triangles[i + 0]) && triangles[i + 0] != index) neighboors.Add(triangles[i + 0]);
                if (!neighboors.Contains(triangles[i + 1]) && triangles[i + 1] != index) neighboors.Add(triangles[i + 1]);
                if (!neighboors.Contains(triangles[i + 2]) && triangles[i + 2] != index) neighboors.Add(triangles[i + 2]);
            }

            return neighboors.ToArray();
        }

        public void CreateGrid3D()
        {
            if (meshFilter == null) return;

            baseMesh = meshFilter.mesh;
            Debug.Log($"Vertices: {meshFilter.sharedMesh.vertexCount}, Triangles: {meshFilter.sharedMesh.triangles.Length}");

            indexBounds?.Clear();
            boundsVertices?.Clear();
            var encapsulatingBounds = new Bounds(meshFilter.sharedMesh.bounds.center, meshFilter.sharedMesh.bounds.size);
            var rayon = encapsulatingBounds.extents;
            var edgeSizes = new Vector3(
                (encapsulatingBounds.size.x) / (gridSize.x),
                (encapsulatingBounds.size.y) / (gridSize.y),
                (encapsulatingBounds.size.z) / (gridSize.z));

            for (int i = 0; i < gridSize.x; i++)
            {
                for (int j = 0; j < gridSize.y; j++)
                {
                    for (int k = 0; k < gridSize.z; k++)
                    {
                        var index = (k * gridSize.x * gridSize.y) + (j * gridSize.x) + i;

                        var center = meshFilter.sharedMesh.bounds.center;
                        var even = new Vector3(gridSize.x % 2 == 0 ? 1 : 0, gridSize.y % 2 == 0 ? 1 : 0, gridSize.z % 2 == 0 ? 1 : 0);
                        center += new Vector3(-rayon.x + i * edgeSizes.x, rayon.y - j * edgeSizes.y, rayon.z - k * edgeSizes.z);
                        center += new Vector3(edgeSizes.x / 2, -edgeSizes.y / 2, -edgeSizes.z / 2);
                        indexBounds?.Add(index, new Bounds(center, edgeSizes));
                    }
                }
            }
        }

        public void Simplify()
        {
            if (meshFilter == null) return;
            
            nVertices.Clear();
            nVertices.AddRange(meshFilter.sharedMesh.vertices);

            nTriangles.Clear();
            nTriangles.AddRange(meshFilter.sharedMesh.triangles);
            //Pour  chaque  triangle  du  maillage,
            //re-indexer ses trois sommets sur les sommets representants de leurs cellules respectives
            //si les 3 cellules sont differentes
            //sinon ́eliminer le triangle

            Dictionary<int, uint> verticesIndexTimes = new Dictionary<int, uint>();
            foreach (var indexBound in indexBounds)
            {
                verticesIndexTimes.Clear();

                // Get all the vertices in the bounds
                var verticesIndex = new List<int>();
                for (int i = 0; i < nVertices.Count; i++)
                {
                    if (indexBound.Value.Contains(nVertices[i]))
                    {
                        nVertices[i] = indexBound.Value.center;
                        if (!verticesIndex.Contains(i))
                        {
                            verticesIndex.Add(i);
                        }
                    }
                }

                if (verticesIndex.Count > 0)
                {
                    var index = verticesIndex[0];
                    verticesIndex.RemoveAt(0);

                    for (int i = 0; i < verticesIndex.Count; i++)
                    {
                        var k = verticesIndex[i];
                        nVertices.RemoveAt(k);

                        for (int j = 0; j < verticesIndex.Count; j++)
                        {
                            verticesIndex[j]--;
                        }

                        for (int j = 0; j < nTriangles.Count; j++)
                        {
                            if (nTriangles[j] == k)
                            {
                                nTriangles[j] = index;
                            }
                            if (nTriangles[j] >= k)
                            {
                                nTriangles[j]--;
                            }
                        }
                    }

                    for (int i = nTriangles.Count - 1; i > 0; i -= 3)
                    {
                        var t0 = nTriangles[i - 0];
                        var t1 = nTriangles[i - 1];
                        var t2 = nTriangles[i - 2];

                        if (t0 == t1 && t1 == t2)
                        {
                            nTriangles.RemoveAt(i - 0);
                            nTriangles.RemoveAt(i - 1);
                            nTriangles.RemoveAt(i - 2);
                        }
                    }

                    verticesIndex.Add(index);
                }

                boundsVertices.Add(indexBound.Value, verticesIndex);

                simplifiedMesh = new Mesh();
                simplifiedMesh.vertices = nVertices.ToArray();
                simplifiedMesh.triangles = nTriangles.ToArray();
                simplifiedMesh.RecalculateNormals();
            }
        }
    }
}
