﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts
{
    public class OffDrawer : MonoBehaviour
    {
        [SerializeField] private Material _material;
        [SerializeField] private string _path;
        [SerializeField] private OffFormat _off;

        private MeshFilter _meshFilter;
        private MeshRenderer _meshRenderer;
        private Mesh _mesh;

        private bool _hasUpdate;

        // Start is called before the first frame update
        void Start()
        {
            _hasUpdate = true;

            _meshFilter = gameObject.AddComponent<MeshFilter>();
            _meshRenderer = gameObject.AddComponent<MeshRenderer>();

            _mesh = new Mesh();
        }

        public void OnValidate()
        {
            if (!_hasUpdate) _hasUpdate = true;
        }

        public void Update()
        {
            if (_hasUpdate)
            {
                if (OffParser.TryParse(_path, out _off))
                {
                    _mesh.Clear();
                    _mesh.vertices = _off.Vertices;
                    _mesh.triangles = _off.Triangles;
                    _mesh.normals = _off.Normals;

                    if (_meshFilter && _mesh)
                    {
                        _meshFilter.mesh = _mesh;
                    }

                    if (_meshRenderer && _material)
                    {
                        _meshRenderer.material = _material;
                    }

                    //_mesh.RecalculateNormals();

                    Debug.Log($"[OffDrawer] Off parsed with success !");
                }
                else
                {
                    Debug.LogError($"[OffDrawer] Off parse failed with path: {_path}");
                }

                _hasUpdate = false;
            }
        }

        public void FixedUpdate()
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                Debug.LogWarning($"Exporting mesh...");
                if (!OffParser.Create($"Assets/Objects/{gameObject.name}", _mesh))
                {
                    Debug.LogError($"Exporting mesh failed..");
                }
                else
                {
                    Debug.Log($"Exporting mesh succeed !");
                }
            }
        }

#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
            //if (_mesh && _mesh.normals != null)
            //{
            //    Gizmos.color = Color.red;
            //    for (int i = 0; i < _mesh.normals.Length; i++)
            //    {
            //        Gizmos.DrawRay(_mesh.vertices[i], _mesh.normals[i]);
            //        //Gizmos.DrawSphere(transform.position + _mesh.vertices[i], 0.01f);
            //        //Handles.Label(transform.position + _mesh.vertices[i] - Vector3.up * .01f, i.ToString());
            //    }
            //}
        }
#endif
    }
}