using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using UnityEngine;
using System.IO;

[System.Serializable]
public struct OffFormat
{
    public bool IsValid;
    public uint NbSummit, NbFace, NbEdge;
    public Vector3[] Vertices;
    public Vector3[] Normals;
    public int[] Triangles;
    [System.Serializable]
    public struct Face
    {
        public uint NbSummit;
        public int[] Triangles;
    }
    public Face[] Faces;
}


public class OffParser
{
    public static bool Create(string path, Mesh mesh)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append($"OFF\n");
        sb.Append($"{mesh.vertexCount} {mesh.triangles.Length / 3} {mesh.triangles.Length * 3}\n");
        
        string[] vertexStr = new string[3];
        foreach (var vertex in mesh.vertices)
        {
            vertexStr[0] = vertex.x.ToString(CultureInfo.InvariantCulture)/*.Replace(',', '.')*/;
            vertexStr[1] = vertex.y.ToString(CultureInfo.InvariantCulture)/*.Replace(',', '.')*/;
            vertexStr[2] = vertex.z.ToString(CultureInfo.InvariantCulture)/*.Replace(',', '.')*/;
            sb.Append($"{vertexStr[0]} {vertexStr[1]} {vertexStr[2]}\n");
        }
        
        string[] triangleStr = new string[3];
        for (int i = 0; i < mesh.triangles.Length / 3; i++)
        {
            int triangle = i * 3;
            triangleStr[0] = mesh.triangles[triangle].ToString(CultureInfo.InvariantCulture)/*.Replace(',', '.')*/;
            triangleStr[1] = mesh.triangles[triangle + 1].ToString(CultureInfo.InvariantCulture)/*.Replace(',', '.')*/;
            triangleStr[2] = mesh.triangles[triangle + 2].ToString(CultureInfo.InvariantCulture)/*.Replace(',', '.')*/;
            sb.Append($"{3} {triangleStr[0]} {triangleStr[1]} {triangleStr[2]}\n");
        }

        var sw = File.CreateText($"{path}.off");
        sw.Write(sb.ToString());
        sw.Close();

        if (!File.Exists($"{path}.off"))
        {
            return false;
        }

        return true;
    }

    public static bool TryParse(string path, out OffFormat off)
    {
        if (!File.Exists(path))
        {
            off = default(OffFormat);
            off.IsValid = false;
            return false;
        }
        
        off = OffParser.Parse(path);
        return true;
    }

    public static OffFormat Parse(string path)
    {
        OffFormat off = new OffFormat();
        List<Vector3> vertices = new List<Vector3>();
        List<OffFormat.Face> faces = new List<OffFormat.Face>();

        var sr = new StreamReader(path);
        string line = null;
        uint lineNumber = 0;

        while (!sr.EndOfStream)
        {
            line = sr.ReadLine();
            if (line != null && lineNumber >= 1)
            {
                var lineSplitBySpace = line.TrimEnd(Environment.NewLine.ToCharArray()).Split(' ');
                if (lineNumber == 1)
                {
                    off.NbSummit = uint.Parse(lineSplitBySpace[0]);
                    off.NbFace = uint.Parse(lineSplitBySpace[1]);
                    off.NbEdge = uint.Parse(lineSplitBySpace[2]);
                }
                else
                {
                    if ((lineNumber - 2) < off.NbSummit)
                    {
                        if (lineSplitBySpace.Length >= 3)
                        {
                            vertices.Add(new Vector3(
                                float.Parse(lineSplitBySpace[0], CultureInfo.InvariantCulture),
                                float.Parse(lineSplitBySpace[1], CultureInfo.InvariantCulture),
                                float.Parse(lineSplitBySpace[2], CultureInfo.InvariantCulture)));
                        }
                    }
                    else
                    {
                        if (lineSplitBySpace.Length >= 4)
                        {
                            faces.Add(new OffFormat.Face
                            {
                                NbSummit = uint.Parse(lineSplitBySpace[0]),
                                Triangles = new[]
                                {
                                    int.Parse(lineSplitBySpace[1]),
                                    int.Parse(lineSplitBySpace[2]),
                                    int.Parse(lineSplitBySpace[3])
                                }
                            });
                        }
                    }
                }
            }

            lineNumber++;
        }

        // Set the triangles
        {
            List<int> triangles = new List<int>();
            foreach (var face in faces)
            {
                foreach (var t in face.Triangles)
                {
                    triangles.Add(t);
                }
            }

            off.Triangles = triangles.ToArray();
        }

        // Center the object and normalize it's size
        {
            var farrestVertex = Vector3.zero;
            Vector3 center = Vector3.zero;
            foreach (var vertex in vertices) center += vertex;
            center /= vertices.Count;

            for (int i = 0; i < vertices.Count; i++)
            {
                Vector3 vertex = vertices[i];
                vertex -= center;
                vertices[i] = vertex;

                if (farrestVertex.sqrMagnitude < vertex.sqrMagnitude)
                {
                    farrestVertex = vertex;
                }
            }

            float norm = farrestVertex.magnitude;
            for (int i = 0; i < vertices.Count; i++)
            {
                Vector3 vertex = vertices[i];
                vertex /= norm;
                vertices[i] = vertex;
            }

            off.Vertices = vertices.ToArray();
            off.Faces = faces.ToArray();
        }

        // Recalculate normals
        {
            Vector3[] normals = new Vector3[off.Vertices.Length];

            int triangleCount = off.Triangles.Length / 3;
            for (int i = 0; i < triangleCount; i++)
            {
                int normalTriangleIndex = i * 3;
                var t1 = off.Triangles[normalTriangleIndex];
                var t2 = off.Triangles[normalTriangleIndex + 1];
                var t3 = off.Triangles[normalTriangleIndex + 2];

                var v1 = off.Vertices[t1];
                var v2 = off.Vertices[t2];
                var v3 = off.Vertices[t3];

                var triangleNormal = Vector3.Cross(v2 - v1, v3 - v1).normalized;

                normals[t1] += triangleNormal;
                normals[t2] += triangleNormal;
                normals[t3] += triangleNormal;
            }

            for (int i = 0; i < normals.Length; i++)
            {
                normals[i].Normalize();
            }

            off.Normals = normals;
        }
        
        if (off.NbSummit > 0 && off.NbFace > 0 && off.NbEdge > 0 
            && off.Vertices.Length > 0 && off.Faces.Length > 0)
        {
            off.IsValid = true;
        }

        return off;
    }
}
