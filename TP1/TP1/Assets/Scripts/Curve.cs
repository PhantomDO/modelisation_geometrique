using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum CurveEnum
{
    Chaikin, Hermite, Bezier, DeCasteljau
}

public class Curve : MonoBehaviour
{
    public LineRenderer lr;
    public CurveEnum algorythm;

    [Header("Chaikin")] 
    public Transform[] path;
    public bool close = false;
    [Range(1, 10)] public uint iterations = 2;
    [Range(0.1f, 0.9f)] public float ratio = .25f;

    [Header("Hermite")] 
    [Range(1, 10)] public uint step = 3;
    [Range(1.0f, 1000.0f)] public float mult = 10.0f;
    public Transform t0; 
    public Transform t1; 
    public Transform v0;
    public Transform v1;

    [Header("Bezier")] 
    [Range(1, 10)] public uint bIterations = 10;

#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        switch (algorythm)
        {
            case CurveEnum.Bezier:
            case CurveEnum.Chaikin:
                if (path.Length > 1)
                {
                    for (int i = 0; i < path.Length - 1; i++)
                    {
                        Gizmos.color = Color.green;
                        Gizmos.DrawLine(path[i].position, path[i + 1].position);
                    }
                }
                break;
            case CurveEnum.Hermite:
                if (t0 && t1)
                {
                    Gizmos.color = Color.green;
                    Gizmos.DrawLine(t0.position, t1.position);
                }
                break;
            case CurveEnum.DeCasteljau:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
#endif

    public void Update()
    {
        if (lr == null || t0 == null || t1 == null) return;
        
        var vs = new List<Vector3>();
        Vector3[] points = new Vector3[] {};
        switch (algorythm)
        {
            case CurveEnum.Chaikin:
                foreach (var t in path) vs.Add(t.position);
                points = Chaikin(vs.ToArray(), close, iterations, ratio);
                break;
            case CurveEnum.Hermite:
                Debug.DrawLine(v0.position, v1.position, Color.gray);
                points = Hermite(t0.position, t1.position, v0.position, v1.position, step, mult);
                break;
            case CurveEnum.Bezier:
                foreach (var t in path) vs.Add(t.position);
                points = Bezier(vs.ToArray(), bIterations);
                break;
            case CurveEnum.DeCasteljau:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        lr.positionCount = points.Length;
        lr.SetPositions(points);
    }

    #region Algorythm

    /// <summary>
    /// 
    /// </summary>
    /// <param name="path">list of positions</param>
    /// <param name="close">is the curve closed</param>
    /// <param name="iterations">nb iterations</param>
    /// <param name="ratio">between 0 & 1 in percent</param>
    /// <returns></returns>
    public Vector3[] Chaikin(Vector3[] path, bool close = false, uint iterations = 1, float ratio = .25f)
    {
        var points = new List<Vector3>();
        points.AddRange(path);
            
        for (uint iter = 0; iter < iterations; iter++)
        {
            var chainkinCurve = new List<Vector3>();
            var numPoints = !close ? points.Count - 1 : points.Count;

            for (int idx = 0; idx < numPoints; idx++)
            {
                var p0 = points[idx];
                var p1 = points[(idx + 1) % points.Count];

                if (ratio > 0.5f) ratio = 1 - ratio;
                var first = Vector3.Lerp(p0, p1, ratio);
                var second = Vector3.Lerp(p1, p0, ratio);
                    
                if (!close && idx == 0)
                {
                    chainkinCurve.Add(p0);
                    chainkinCurve.Add(second);
                }
                else if (!close && idx == numPoints - 1)
                {
                    chainkinCurve.Add(first);
                    chainkinCurve.Add(p1);
                }
                else
                {
                    chainkinCurve.Add(first);
                    chainkinCurve.Add(second);
                }
            }

            points.Clear();
            points.AddRange(chainkinCurve);
        }

        return points.ToArray();
    }

    /// <summary>
    /// Hermite curve : a*u^3 + b*u^2 + c*u + d
    /// </summary>
    /// <param name="p0">starting point</param>
    /// <param name="p1">ending point</param>
    /// <param name="v0">tan start</param>
    /// <param name="v1">tan end</param>
    /// <returns></returns>
    public Vector3[] Hermite(Vector3 p0, Vector3 p1, Vector3 v0, Vector3 v1, uint step = 1, float mult = 1f)
    {
        var lst = new List<Vector3>();

        v0 *= mult;
        v1 *= mult;

        var a = (2 * p0) - (2 * p1) + v0 + v1;
        var b = (-3 * p0) + (3 * p1) - (2 * v0) - v1;
        var c = v0;
        var d = p0;
        
        float factor = 1.0f / (step - 1);
        for (float u = 0.0f; u <= 1.0f; u += factor)
        {
            var u2 = u * u;
            var u3 = u2 * u;
            var pU = (a * u3) + (b * u2) + (c * u) + d;
            lst.Add(pU);
        }

        lst.Add(p1);

        return lst.ToArray();
    }

    public Vector3[] Bezier(Vector3[] path, uint step)
    {
        var lst = new List<Vector3>();
        float factor = 1.0f / step;
        for (float u = 0.0f; u <= 1.0f; u += factor)
        {
            var pU = path[0];
            int n = path.Length - 1;
            for (int i = 0; i <= n; i++)
            {
                pU += Bernstein(u, i, n) * path[i];
            }
            lst.Add(pU);
        }
        lst.Add(path[path.Length - 1]);
        return lst.ToArray();
    }
    
    #endregion

    #region Utilities

    private int Factorial(int n)
    {
        int ret = 1;
        while (n > 1) ret *= n--;
        return ret;
    }

    private float Bernstein(float u, int i, int n)
    {
        var a = Factorial(n) / (Factorial(i) * Factorial(n - i));
        var b = Mathf.Pow(u, i);
        var c = Mathf.Pow(1.0f - u, n - i);
        return a * b * c;
    }

    #endregion
}
