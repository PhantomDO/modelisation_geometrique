using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public enum PrimitiveEnum
{
    Triangle,
    Cylinder,
    Cone,
    Sphere,
}

public class PrimitiveDrawer : MonoBehaviour
{
    [SerializeField] private Material _material;
    [SerializeField] private Vector3 _startPosition = Vector3.zero;
    [SerializeField] private PrimitiveEnum _selectedPrimitive;

    [SerializeField] private Vector3[] _vertices;
    [SerializeField] private int[] _triangles;

    [Header("Triangles")]
    [SerializeField] private Vector2Int _triDimension;

    [Header("Cylinder")]
    [SerializeField] private float _cylinderHeight;
    [SerializeField] private float _cylinderRay;
    [SerializeField] private int _cylinderMeridian;

    [Header("Cone")]
    [SerializeField, Min(0.00001f)] private float _coneHeight;
    [SerializeField, Min(0.00001f)] private float _coneTruncHeight;
    [SerializeField] private float _coneBottomRay;
    [SerializeField] private float _coneTopRay;
    [SerializeField] private int _coneMeridian;

    [Header("Sphere")]
    [SerializeField] private float _sphereRay; 
    [SerializeField] private Vector2Int _sphereDimension; 

    private MeshFilter _meshFilter;
    private MeshRenderer _meshRenderer;
    private Mesh _mesh;

    private bool _hasUpdate;

    #region Unity 

    // Start is called before the first frame update
    void Start()
    {
        _hasUpdate = true;

        _vertices = null;
        _triangles = null;

        _meshFilter = gameObject.AddComponent<MeshFilter>();
        _meshRenderer = gameObject.AddComponent<MeshRenderer>();

        _mesh = new Mesh();
    }

    public void OnValidate()
    {
        if (!_hasUpdate) _hasUpdate = true;
    }

#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        if (_mesh && _mesh.vertices != null)
        {
            Gizmos.color = Color.red;
            //for (int i = 0; i < _mesh.vertices.Length; i++)
            //{
            //    Gizmos.DrawSphere(transform.position + _mesh.vertices[i], 0.01f);
            //    Handles.Label(transform.position + _mesh.vertices[i] - Vector3.up * .01f, i.ToString());
            //}
        }
    }
#endif

    public void Update()
    {
        if (_hasUpdate)
        {
            switch (_selectedPrimitive)
            {
                case PrimitiveEnum.Triangle:
                    DrawTriangles(_triDimension.x, _triDimension.y);
                    break;
                case PrimitiveEnum.Cylinder:
                    DrawCylinder(_cylinderRay, _cylinderHeight, _cylinderMeridian);
                    break;
                case PrimitiveEnum.Cone:
                    DrawCone(_coneBottomRay, _coneHeight, _coneMeridian, _coneTopRay, _coneTruncHeight);
                    break;
                case PrimitiveEnum.Sphere:
                    DrawSphere(_sphereRay, _sphereDimension.x, _sphereDimension.y);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            _hasUpdate = false;
        }
    }
    
    #endregion

    private void ApplyMeshParameters()
    {
        _mesh.Clear();

        _mesh.vertices = _vertices;
        _mesh.triangles = _triangles;

        if (_meshFilter && _mesh)
        {
            _meshFilter.mesh = _mesh;
        }

        if (_meshRenderer && _material)
        {
            _meshRenderer.material = _material;
        }

        _mesh.RecalculateNormals();
    }

    public void DrawTriangle()
    {
        _vertices = new Vector3[3];
        _triangles = new int[3];

        _vertices[0] = new Vector3(0, 0, 0);
        _vertices[1] = new Vector3(1, 0, 0);
        _vertices[2] = new Vector3(0, 1, 0);

        _triangles[0] = 0;
        _triangles[1] = 1;
        _triangles[2] = 2;

        ApplyMeshParameters();
    }

    public void DrawTriangles(int row, int column)
    {
        _vertices = new Vector3[(row + 1) * (column + 1)];

        int i = 0;
        for (int y = 0; y <= column; y++)
        {
            for (int x = 0; x <= row; x++)
            {
                _vertices[i] = _startPosition + new Vector3(x, y);
                i++;
            }
        }

        _triangles = new int[row * column * 3 * 2];

        int ti = 0, vi = 0;
        for (int y = 0; y < column; y++)
        {
            for (int x = 0; x < row; x++)
            {
                _triangles[ti] = vi;
                _triangles[ti + 3] = _triangles[ti + 2] = vi + 1;
                _triangles[ti + 4] = _triangles[ti + 1] = vi + row + 1;
                _triangles[ti + 5] = vi + row + 2;

                ti += 6;
                vi++;
            }

            vi++;
        }

        ApplyMeshParameters();
    }

    public void DrawCylinder(float r, float h, int meridian)
    {
        // cylindre = revolution d'axe D et rayon r
        // constitu� ensemble pts IRcube � distance r de la droite D
        // equation = x� + y� = r�
        // plan d�fini par h : -h / 2 && h / 2

        _vertices = new Vector3[(meridian) * 2 + 2];

        _vertices[0] = _startPosition - new Vector3(0, h / 2, 0);

        for (int y = 0; y < 2; y++)
        {
            for (int x = 0; x < meridian; x++)
            {
                float theta = (2f * Mathf.PI) * (float)(x == meridian ? 0 : x) / meridian;

                _vertices[x + y * (meridian) + 1] = new Vector3(
                    r * Mathf.Cos(theta),
                    (y == 0 ? -h : h) / 2,
                    r * Mathf.Sin(theta));
            }
        }

        _vertices[_vertices.Length - 1] = _startPosition + new Vector3(0, h / 2, 0);

        _triangles = new int[(meridian * 2 + 1) * 3 * 2];

        int ti = 0;

        // do the bottom of the cylinder
        for (int bot = 0; bot < meridian; bot++)
        {
            _triangles[ti++] = 0;
            _triangles[ti++] = bot + 1;
            _triangles[ti++] = (bot + 2) % (meridian + 1) + (bot == meridian - 1 ? 1 : 0);
        }

        // do the revolution of the cylinder
        for (int mid = 0; mid < meridian; mid++)
        {
            _triangles[ti] = mid + (mid == 0 ? meridian : 0);
            _triangles[ti + 3] = _triangles[ti + 2] = ((mid + 1) % (meridian + 1));
            _triangles[ti + 4] = _triangles[ti + 1] = meridian + ((mid + 1) % (meridian + 1));
            _triangles[ti + 5] = meridian + ((mid + 2) % (meridian + 1) + (mid == meridian - 1 ? 1 : 0));

            ti += 6;
        }

        //// do the top of the cylinder
        for (int top = 0; top < meridian; top++)
        {
            _triangles[ti++] = _vertices.Length - 1;
            _triangles[ti++] = _vertices.Length - 1 - (top + 1);
            _triangles[ti++] = _vertices.Length - 1 - ((top + 2) % (meridian + 1) + (top == meridian - 1 ? 1 : 0));
        }

        ApplyMeshParameters();
    }

    public void DrawCone(float botRadius, float totalHeight, int meridian, 
        float topRadius = 0.0f, float truncHeight = 0.0f)
    {
        // cylindre = revolution d'axe D et rayon r
        // constitu� ensemble pts IRcube � distance r de la droite D
        // equation = x� + y� = r�
        // plan d�fini par h : -h / 2 && h / 2

        int slice = topRadius <= 0.0f || truncHeight <= 0.0f ? 1 : 2;

        _vertices = new Vector3[(meridian) * slice + 2];

        _vertices[0] = _startPosition - new Vector3(0, totalHeight / 2, 0);


        for (int y = 0; y < slice; y++)
        {
            float r = y == 0 ? botRadius : topRadius;
            for (int x = 0; x < meridian; x++)
            {
                float theta = (2f * Mathf.PI) * (float)(x == meridian ? 0 : x) / meridian;
                
                _vertices[x + y * (meridian) + 1] = new Vector3(
                    r * Mathf.Cos(theta),
                    (y == 0 ? -totalHeight * 0.5f: (totalHeight / 2) * Mathf.Max(truncHeight / totalHeight)),
                    r * Mathf.Sin(theta));
            }
        }

        _vertices[_vertices.Length - 1] = new Vector3(0, (totalHeight / 2) * (truncHeight <= 0.0f ? 1 : truncHeight / totalHeight), 0);
        
        _triangles = new int[(meridian * 2 + 1) * 3 * 2];

        int ti = 0;

        // do the bottom of the cylinder
        for (int bot = 0; bot < meridian; bot++)
        {
            _triangles[ti++] = 0;
            _triangles[ti++] = bot + 1;
            _triangles[ti++] = (bot + 2) % (meridian + 1) + (bot == meridian - 1 ? 1 : 0);
        }

        // do the revolution of the cylinder
        for (int mid = 0; mid < meridian; mid++)
        {
            _triangles[ti] = mid + (mid == 0 ? meridian : 0);
            _triangles[ti + 3] = _triangles[ti + 2] = ((mid + 1) % (meridian + 1));
            _triangles[ti + 4] = _triangles[ti + 1] = meridian + ((mid + 1) % (meridian + 1)) ;
            _triangles[ti + 5] = meridian + ((mid + 2) % (meridian + 1) + (mid == meridian - 1 ? 1 : 0));

            ti += 6;
        }

        //// do the top of the cylinder
        for (int top = 0; top < meridian; top++)
        {
            _triangles[ti++] = _vertices.Length - 1;
            _triangles[ti++] = _vertices.Length - 1 - (top + 1);
            _triangles[ti++] = _vertices.Length - 1 - ((top + 2) % (meridian + 1) + (top == meridian - 1 ? 1 : 0));
        }

        ApplyMeshParameters();
    }

    /// <summary>
    /// http://wiki.unity3d.com/index.php/ProceduralPrimitives
    /// </summary>
    /// <param name="r"></param>
    /// <param name="h"></param>
    /// <param name="meridian"></param>
    /// <param name="slice"></param>
    public void DrawSphere(float r, int meridian, int slice)
    {
        _vertices = new Vector3[(meridian) * slice + 2];
        
        _vertices[0] = Vector3.up * r;
        for (int y = 0; y < slice; y++)
        {
            float phi = Mathf.PI * (float)(y + 1) / (slice + 1);
            for (int x = 0; x < meridian; x++)
            {
                float theta = (2f * Mathf.PI) * (float)(x == meridian ? 0 : x) / meridian;

                _vertices[x + y * (meridian) + 1] = r * new Vector3(
                    Mathf.Sin(phi) * Mathf.Cos(theta),
                    Mathf.Cos(phi), 
                    Mathf.Sin(phi) * Mathf.Sin(theta));
            }
        }
        _vertices[_vertices.Length - 1] = Vector3.up * -r;

        _triangles = new int[(_vertices.Length + 1) * 3 * 2];

        int ti = 0;
        for (int top = 0; top < meridian; top++)
        {
            _triangles[ti++] = (top + 2) % (meridian + 1) + (top == meridian - 1 ? 1 : 0);
            _triangles[ti++] = top + 1;
            _triangles[ti++] = 0;
        }

        for (int y = 0; y < slice - 1; y++)
        {
            for (int x = 0; x < meridian; x++)
            {
                _triangles[ti] = x + (x == 0 ? meridian : 0) + (meridian * y);
                _triangles[ti + 4] = _triangles[ti + 1] = ((x + 1) % (meridian + 1)) + (meridian * y);
                _triangles[ti + 3] = _triangles[ti + 2] = meridian + ((x + 1) % (meridian + 1)) + (meridian * y);
                _triangles[ti + 5] = meridian + ((x + 2) % (meridian + 1) + (x == meridian - 1 ? 1 : 0)) + (meridian * y);
                
                ti += 6;
            }
        }

        for (int bot = 0; bot < meridian; bot++)
        {
            _triangles[ti++] = _vertices.Length - 1;
            _triangles[ti++] = _vertices.Length - 1 - (bot + 2) % (meridian + 1) - (bot == meridian - 1 ? 1 : 0);
            _triangles[ti++] = _vertices.Length - 1 - (bot + 1);
        }

        ApplyMeshParameters();
    }
}
