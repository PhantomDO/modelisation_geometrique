﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts
{
    [System.Serializable]
    public class Index3DVoxelDictionary : SerializableDictionary<int, Voxel> {}
    [System.Serializable]
    public class VoxelPotentialDictionary : SerializableDictionary<Voxel, float> {}
    [System.Serializable]
    public class Index3DGameObjectDictionary : SerializableDictionary<int, GameObject> {}

    //[System.Serializable]
    //public class Octree
    //{
    //    [SerializeField] private int _deep;
    //    [SerializeField] private Voxel _overlapingVoxel;
    //    [SerializeField] private Voxel[] _voxels;

    //    public Voxel OverlapingVoxel => _overlapingVoxel;
    //    public Voxel[] Voxels => _voxels;
    //    public int Deep => _deep;

    //    public Octree(Voxel overlapingVoxel, int deep)
    //    {
    //        _overlapingVoxel = overlapingVoxel;
    //        _deep = deep;
    //    }

    //    public void SetDeepVoxel()
    //    {
    //        float width = _overlapingVoxel.Width.x / 2;
    //        float wN = -width / 2;
    //        float wP = width / 2;

    //        var v = new Vector3[]
    //        {
    //            new Vector3(wN, wN, wN),
    //            new Vector3(wN, wP, wN),
    //            new Vector3(wP, wP, wN),
    //            new Vector3(wP, wN, wN),
    //            new Vector3(wN, wN, wP),
    //            new Vector3(wN, wP, wP),
    //            new Vector3(wP, wP, wP),
    //            new Vector3(wP, wN, wP)
    //        };

    //        _voxels = new Voxel[8]
    //        {
    //            new Voxel(_overlapingVoxel.Center + new Vector3(wN, wN, wN), width * Vector3.one, 0),
    //            new Voxel(_overlapingVoxel.Center + new Vector3(wN, wP, wN), width * Vector3.one, 1),
    //            new Voxel(_overlapingVoxel.Center + new Vector3(wP, wP, wN), width * Vector3.one, 2),
    //            new Voxel(_overlapingVoxel.Center + new Vector3(wP, wN, wN), width * Vector3.one, 3),
    //            new Voxel(_overlapingVoxel.Center + new Vector3(wN, wN, wP), width * Vector3.one, 4),
    //            new Voxel(_overlapingVoxel.Center + new Vector3(wN, wP, wP), width * Vector3.one, 5),
    //            new Voxel(_overlapingVoxel.Center + new Vector3(wP, wP, wP), width * Vector3.one, 6),
    //            new Voxel(_overlapingVoxel.Center + new Vector3(wP, wN, wP), width * Vector3.one, 7),
    //        };
    //    }
    //}

    public class Voxelizer : MonoBehaviour
    {
        [SerializeField] private float _refreshDelay = .75f;
        [SerializeField] private float _defaultThreshold = .5f;
        [SerializeField] private Vector3Int _numberCubes;
        [SerializeField] private Material _material;
        [SerializeField] private List<MeshFilter> _meshFilters;

        [System.Serializable] private enum MergeOperator { Union, Intersect }
        [SerializeField] private MergeOperator _mergeOperator;
        
        [SerializeField] private Index3DVoxelDictionary _voxels;
        [SerializeField] private VoxelPotentialDictionary _potentials;
        [SerializeField] private Index3DGameObjectDictionary _gameObjects;
        
        private Bounds _encapsulatingBounds;
        private Vector3[] _previousMeshPositions;
        private Vector3Int _previousNumberCubes;
        private List<MeshFilter> _previousMeshFilters;
        private float[] _timeSinceLastUpdate;

        private enum GameLoop { Start, Update, Lock }
        private GameLoop _hasUpdate;

        #region Unity

        void Start()
        {
            _previousNumberCubes = _numberCubes;
            _previousMeshFilters = _meshFilters;
            
            _voxels = new Index3DVoxelDictionary();
            _potentials = new VoxelPotentialDictionary();
            _gameObjects = new Index3DGameObjectDictionary();
            _timeSinceLastUpdate = new float[_meshFilters.Count];
            _previousMeshPositions = new Vector3[_meshFilters.Count];

            _hasUpdate = GameLoop.Start;
        }

        public void OnValidate()
        {
            if (_hasUpdate == GameLoop.Lock)
            {
                _hasUpdate = GameLoop.Update;
            }

            if (_previousMeshFilters != null && _meshFilters != null && _previousMeshFilters.Count != _meshFilters.Count)
            {
                _timeSinceLastUpdate = new float[_meshFilters.Count];
                _previousMeshPositions = new Vector3[_meshFilters.Count];
            }

            if (_previousNumberCubes != _numberCubes)
            {
                _previousNumberCubes = _numberCubes;
                _hasUpdate = GameLoop.Start;
            }
        }

        public void Update()
        {
            _previousNumberCubes = _numberCubes;
            for (int i = 0; i < _meshFilters.Count; i++)
            {
                _previousMeshFilters[i] = _meshFilters[i];
                if (Vector3.Distance(_previousMeshPositions[i], _meshFilters[i].transform.position) >= 0.00001f)
                {
                    _timeSinceLastUpdate[i] = Time.timeSinceLevelLoad;
                    _previousMeshPositions[i] = _meshFilters[i].transform.position;
                }

                if (_timeSinceLastUpdate[i] - Time.timeSinceLevelLoad + Time.time >= _refreshDelay)
                {
                    _timeSinceLastUpdate[i] = 0.0f;
                    _hasUpdate = GameLoop.Update;
                    break;
                }
            }

            if (_hasUpdate != GameLoop.Lock)
            {
                _voxels?.Clear();
                _potentials?.Clear();
                _gameObjects?.Clear();
                
                foreach (Transform tr in transform)
                {
                    Destroy(tr.gameObject);
                }

                Voxelize();
                _hasUpdate = GameLoop.Lock;
            }
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(_encapsulatingBounds.center, 0.05f);
            Gizmos.DrawWireCube(_encapsulatingBounds.center, _encapsulatingBounds.size);

            if (_voxels != null && _voxels.Count > 0 && 
                _potentials != null && _potentials.Count > 0 &&
                _gameObjects != null && _gameObjects.Count > 0)
            {
                Gizmos.color = Color.gray;

                foreach (var kvp in _voxels)
                {
                    UnityEditor.Handles.Label(
                        _gameObjects[kvp.Key].transform.position, 
                        $"{_potentials[kvp.Value]}",
                        new GUIStyle(GUI.skin.label)
                        {
                            alignment = TextAnchor.MiddleLeft,
                            margin = new RectOffset(),
                            padding = new RectOffset(),
                            fontSize = 15,
                            fontStyle = FontStyle.Bold
                        });
                    Gizmos.DrawWireCube(kvp.Value.Center, kvp.Value.Width);
                }
            }
        }
#endif

        #endregion

        #region Merge Operator

        private bool InSphereRadius(MeshFilter[] meshFilters, Vector3 center, out int intersect)
        {
            intersect = 0;
            for (int i = 0; i < meshFilters.Length; i++)
            {
                var ray = meshFilters[i].sharedMesh.bounds.extents.x;
                if ((meshFilters[i].transform.position - center).sqrMagnitude - (ray * ray) <= 0)
                {
                    intersect++;
                }
            }

            return intersect > 0;
        }
        
        private bool Union(MeshFilter[] meshFilters, Vector3 center)
        {
            return InSphereRadius(meshFilters, center, out int intersect);
        }

        private bool Intersect(MeshFilter[] meshFilters, Vector3 center)
        {
            return InSphereRadius(meshFilters, center, out int intersect) && meshFilters.Length == intersect;
        }

        #endregion

        #region Potentials

        private int VoxelInRadius(Vector3 position, float minDist)
        {
            int voxelInRadius = -1;
            float distance = minDist;
            foreach (var kvp in _voxels)
            {
                var d = Vector3.Distance(kvp.Value.Center, position);
                if (d < distance)
                {
                    distance = d;
                    voxelInRadius = kvp.Key;
                }
            }

            return voxelInRadius;
        }

        public void AddPotential(Vector3 position, float potential, float minDist = 1.0f)
        {
            var index = VoxelInRadius(position, minDist);
            if (index != -1 && _potentials?.Count > 0)
            {
                _potentials[_voxels[index]] = (_potentials[_voxels[index]] + potential) >= 1.0f 
                    ? 1.0f : (_potentials[_voxels[index]] + potential);

                if (_gameObjects[index].TryGetComponent(out MeshRenderer mr))
                {
                    Color c = mr.material.color;
                    c.a = _potentials[_voxels[index]];
                    mr.material.color = c;
                }
            }
        }

        public void RemovePotential(Vector3 position, float potential, float minDist = 1.0f)
        {
            var index = VoxelInRadius(position, minDist);
            if (index != -1 && _potentials?.Count > 0)
            {
                _potentials[_voxels[index]] = (_potentials[_voxels[index]] - potential) <= 0.0f 
                    ? 0.0f : (_potentials[_voxels[index]] - potential);

                if (_gameObjects[index].TryGetComponent(out MeshRenderer mr))
                {
                    Color c = mr.material.color;
                    c.a = _potentials[_voxels[index]];
                    mr.material.color = c;
                }
            }
        }

        #endregion

        private void Voxelize()
        {
            Vector3 size = _meshFilters[0].sharedMesh.bounds.size;
            Vector3 center = _previousMeshPositions[0] = _meshFilters[0].transform.position;

            if (_meshFilters.Count > 1)
            {
                var first = _meshFilters[0];
                Vector3 min = first.transform.position - first.sharedMesh.bounds.extents;
                Vector3 max = first.transform.position + first.sharedMesh.bounds.extents;
                for (int i = 0; i < _meshFilters.Count - 1; i++)
                {
                    var current = _meshFilters[i];
                    var next = _meshFilters[i + 1];

                    // Calculate center of bounding box
                    float dist = Vector3.Distance(current.transform.position, next.transform.position);
                    Vector3 dir = (next.transform.position - current.transform.position).normalized;
                    center += next.transform.position/*dir * (dist / 2)*/;

                    // Calculate min size of bounding box
                    min.x = min.x <= next.transform.position.x ? min.x : next.transform.position.x;
                    min.y = min.y <= next.transform.position.y ? min.y : next.transform.position.y;
                    min.z = min.z <= next.transform.position.z ? min.z : next.transform.position.z;
                    min -= first.sharedMesh.bounds.extents;

                    max.x = max.x >= next.transform.position.x ? max.x : next.transform.position.x;
                    max.y = max.y >= next.transform.position.y ? max.y : next.transform.position.y;
                    max.z = max.z >= next.transform.position.z ? max.z : next.transform.position.z;
                    max += first.sharedMesh.bounds.extents;
                }
                
                center /= _meshFilters.Count;
                size = new Vector3((max.x - min.x), (max.y - min.y), (max.z - min.z));

                ///size += first.sharedMesh.bounds.size;
            }

            _encapsulatingBounds = new Bounds(center, size);
                
            Vector3Int nbCube = _numberCubes;
            Vector3 rayon = _encapsulatingBounds.extents;
            var sizeEdges = new Vector3(
                (_encapsulatingBounds.size.x) / nbCube.x, 
                (_encapsulatingBounds.size.y) / nbCube.y, 
                (_encapsulatingBounds.size.z) / nbCube.z 
            );

            for (int z = 0; z < nbCube.z; z++)
            {
                for (int y = 0; y < nbCube.y; y++)
                {
                    for (int x = 0; x < nbCube.x; x++)
                    {
                        // Create cube from index x,y,z 
                        var width = sizeEdges;
                        var posCenter = center; 
                        posCenter += new Vector3(-rayon.x + x * width.x, rayon.y - y * width.y, rayon.z - z * width.z);
                        posCenter += new Vector3(width.x / nbCube.x,-width.y / nbCube.y,-width.z / nbCube.z);
                        var index = (z * nbCube.x * nbCube.y) + (y * nbCube.x) + x;

                        bool visible = false;
                        switch (_mergeOperator)
                        {
                            case MergeOperator.Union:
                                visible = Voxel.IsVisible(_meshFilters.ToArray(), (filters) => Union(filters, posCenter));
                                break;
                            case MergeOperator.Intersect:
                                visible = Voxel.IsVisible(_meshFilters.ToArray(), (filters) => Intersect(filters, posCenter));
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }

                        if (visible)
                        {
                            var voxel = new Voxel(posCenter, width);
                            _voxels.Add(index, voxel);
                            _potentials.Add(voxel, _defaultThreshold);

                            if (_gameObjects.ContainsKey(index))
                            {
                                var updateGo = voxel.GetVoxel(index, _gameObjects[index]);
                                _gameObjects[index] = updateGo;
                            }
                            else
                            {
                                var go = voxel.GetVoxel(index, null);
                                go.transform.position = posCenter;
                                //go.transform.localScale = new Vector3(1 / sizeEdges.x, 1 / sizeEdges.y, 1 / sizeEdges.z) * sizeEdges.x;
                                go.transform.parent = transform;
                                if (go.TryGetComponent(out MeshRenderer mr))
                                {
                                    mr.material = _material;
                                }

                                if (go.TryGetComponent(out BoxCollider col))
                                {
                                    col.size = sizeEdges;
                                }

                                _gameObjects.Add(index, go);
                            }
                        }
                    }
                }
            }
        }

    }
}